import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">
            CarCar
          </NavLink>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li>
                <NavLink className="navbar-brand" to="/sales_history">
                  Sales History
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/sales">
                  Sales
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/add_salesperson">
                  Add a Salesperson
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/list_salesperson">
                  List a Salesperson
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/add_customer">
                  Add a Customer
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/list_customer">
                  List a Customer
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/sales_record">
                  Add Sales
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/list_manufacturer">
                  List a Manufacturer
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/add_manufacturer">
                  Add a Manufacturer
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li>
                <NavLink className="navbar-brand" to="/list_automobile">
                  List an Automobile
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/add_automobile">
                  Add an Automobile
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/models">
                  Models
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/models/new">
                  Create a Model
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/technicians">
                  Technicians
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/technicians/new">
                  Add a Technician
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/appointments">
                  Service Appointments
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/appointments/new">
                  Create a Service Appointment
                </NavLink>
              </li>
              <li>
                <NavLink className="navbar-brand" to="/appointments/history">
                  Service History List
                </NavLink>
              </li>
              <li>
                <NavLink
                  className="navbar-brand"
                  to="/appointments/searchbyvin"
                >
                  Service Appointments by VIN
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Nav;
