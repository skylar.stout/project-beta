import React, { useEffect, useState } from "react";

function AddAutomobiles() {
  const [automobile, setAddAutomobiles] = useState([]);
  const [color, setColor] = useState("");
  const [year, setYear] = useState("");
  const [vin, setVin] = useState("");
  const [model_id, setModel] = useState("");
  const [models, setModels] = useState([]);

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      color,
      year,
      vin,
      model_id,
    };

    const addautomobilesUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(addautomobilesUrl, fetchConfig);
    if (response.ok) {
      const NewAutomobile = await response.json();
      setColor("")
      setYear("")
      setVin("")
      setModel("")
    }
    else if (response.status === 400) {
      alert("Request failed. Check fields.");
    }
  }

  async function fetchModels(){
    const modelsUrl = "http://localhost:8100/api/models/";
    const modelresponse = await fetch(modelsUrl);

    if (modelresponse.ok) {
      const data = await modelresponse.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    fetchModels();
  }, []);

  function handleChangeColor(event) {
    const { value } = event.target;
    setColor(value);
  }

  function handleChangeYear(event) {
    const { value } = event.target;
    setYear(value);
  }

  function handleChangeVin(event) {
    const { value } = event.target;
    setVin(value);
  }

  function handleChangeModels(event) {
    const { value } = event.target;
    setModel(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h2>Add an automobile to inventory</h2>
          <form onSubmit={handleSubmit} id="add-automobile-form">
            <div className="form-floating mb-3">
              <input
                value={color}
                onChange={handleChangeColor}
                placeholder="color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={year}
                onChange={handleChangeYear}
                placeholder="year"
                required
                type="text"
                name="year"
                id="year"
                className="form-control"
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={vin}
                onChange={handleChangeVin}
                placeholder="vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
              <select
                value={model_id}
                onChange={handleChangeModels}
                required
                name="model"
                id="model"
                className="form-select">
                <option value="">Choose a model</option>
                {models?.map((model) => {
                  return (
                    <option key={model.id} value={model.id}>
                      {model.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddAutomobiles;
