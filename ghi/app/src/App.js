import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SalesPersonHistory from "./SalesPersonHistory";
import ListAllSales from "./ListAllSales";
import AddSalesperson from "./AddSalesperson";
import ListAllSalesPeople from "./ListAllSalesPeople";
import AddCustomer from "./AddCustomer";
import ListAllCustomers from "./ListAllCustomers";
import RecordNewSale from "./RecordNewSale";
import ListManufacturers from "./ListManufacturer";
import AddManufacturer from "./AddManufacturer";
import ListAutomobiles from "./ListAutomobiles";
import AddAutomobiles from "./AddAutomobile";
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import AppointmentForm from './AppointmentForm';
import ServiceHistoryList from './ServiceHistoryList';
import ServiceAppointmentsbyVin from './ServiceAppointmentsbyVin';
import AppointmentList2 from './AppointmentList2';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/sales_history" element={<SalesPersonHistory />} />
          <Route path="/sales" element={<ListAllSales />} />
          <Route path="/add_salesperson" element={<AddSalesperson />} />
          <Route path="/list_salesperson" element={<ListAllSalesPeople />} />
          <Route path="/add_customer" element={<AddCustomer />} />
          <Route path="/list_customer" element={<ListAllCustomers />} />
          <Route path="/sales_record" element={<RecordNewSale />} />
          <Route path="/list_manufacturer" element={<ListManufacturers />} />
          <Route path="/add_manufacturer" element={<AddManufacturer />} />
          <Route path="/list_automobile" element={<ListAutomobiles />} />
          <Route path="/add_automobile" element={<AddAutomobiles />} />
          <Route path="models">
              <Route index element={<ModelList />} />
              <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="technicians">
              <Route index element={<TechnicianList />} />
              <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
              <Route index element={<AppointmentList2 />} />
              <Route path="new" element={<AppointmentForm />} />
              <Route path="history" element={<ServiceHistoryList />} />
              <Route path="searchbyvin" element={<ServiceAppointmentsbyVin />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
